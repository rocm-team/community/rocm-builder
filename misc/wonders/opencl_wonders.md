# OpenCL wonders

OpenCL in apt seems in a pretty dire state if you ask me

```txt
maxzor@mada:~$ apt show ocl-icd-libopencl1
Package: ocl-icd-libopencl1
Version: 2.2.11-1ubuntu1
Priority: extra
Section: libs
Source: ocl-icd
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Vincent Danjean <vdanjean@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 117 kB
Provides: libopencl-1.1-1, libopencl-1.2-1, libopencl-2.0-1, libopencl-2.1-1, libopencl1
Depends: libc6 (>= 2.4)
Suggests: opencl-icd
```

**opencl-icd**
virtual package provided by intel-opencl-icd, mesa-opencl-icd, beignet-opencl-icd, nvidia-tesla-418-opencl-icd, nvidia-tesla-460-opencl-icd, nvidia-tesla-450-opencl-icd, nvidia-opencl-icd, pocl-opencl-icd, nvidia-legacy-390xx-opencl-icd

```txt
Conflicts: amd-app, libopencl1, nvidia-libopencl1-dev
Replaces: amd-app, libopencl1, nvidia-libopencl1-dev
Homepage: https://forge.imag.fr/projects/ocl-icd/
Task: kubuntu-desktop, xubuntu-desktop, lubuntu-desktop, ubuntustudio-desktop, ubuntukylin-desktop, ubuntu-mate-core, ubuntu-mate-desktop, ubuntu-budgie-desktop
Download-Size: 30.3 kB
APT-Manual-Installed: no
APT-Sources: http://fr.archive.ubuntu.com/ubuntu focal/main amd64 Packages
Description: Generic OpenCL ICD Loader
OpenCL (Open Computing Language) is a multivendor open standard for
general-purpose parallel programming of heterogeneous systems that include
CPUs, GPUs and other processors.
.
This package contains an installable client driver loader (ICD Loader)
library that can be used to load any (free or non-free) installable client
driver (ICD) for OpenCL. It acts as a demultiplexer so several ICD can
be installed and used together.

maxzor@mada:~$ apt show amd-app
Package: amd-app
State: not a real package (virtual)
N: Can't select candidate version from package amd-app as it has no candidate
N: Can't select versions from package 'amd-app' as it is purely virtual
N: No packages found

maxzor@mada:~$ apt show libopencl1
Package: libopencl1
State: not a real package (virtual)
N: Can't select candidate version from package libopencl1 as it has no candidate
N: Can't select versions from package 'libopencl1' as it is purely virtual
N: No packages found
```

From https://packages.debian.org/stable/virtual/
libopencl1
virtual package provided by nvidia-libopencl1, ocl-icd-libopencl1
**Why is libopencl name referring to opencl ICD loaders instead of opencl implementations?**

```txt
maxzor@mada:~$ apt show nvidia-libopencl1-dev
Package: nvidia-libopencl1-dev
State: not a real package (virtual)
N: Can't select candidate version from package nvidia-libopencl1-dev as it has no candidate
N: Can't select versions from package 'nvidia-libopencl1-dev' as it is purely virtual
N: No packages found
```
