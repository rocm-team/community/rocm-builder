# 1. wonders for AMD

# 2. wonders for Debian DM-DDs

---

# 1. wonders for AMD

Generally way too much cmake.
Is it welcome to patch out the CPackDeb infrastructure so as to get
in the Debian way? CPackDeb feels very limited compared to the specialized
packaging tooling that Debian have.

## general

What decides what is in the ROCm manifest (default.xml in ROCm repo)
and what is not?

Generally not FHS-compliant to install in /opt/rocm

Will it be possible to compile all without llvm nor clang forks soon?

## comgr (repo compiler-support)

Comgr : build errors that seem to have been patched, but not yet released. not
all #include "llvm/MC/TargetRegistry.h" does not compile against llvm-13 testing
Ugly hack works

## hip

Dependencies:

- rocm-opencl-runtime
- rocclr
- hipcc
- ... (not figured out yet)

Why do you need rocm-opencl to build hip?

Too much cmake:

```
maxzor@maxdeb:~/rocm/try_raw_hip_build$ ls
hip  hipamd  ROCclr  ROCm-OpenCL-Runtime

maxzor@maxdeb:~/rocm/try_raw_hip_build$ cloc .
---------------------------------------------------------------------------------------

Language                             files          blank        comment           code
---------------------------------------------------------------------------------------
C++                                    808          34860          39777         185352
C/C++ Header                           465          25517          30066         140971
CMake                                 1007          11651          29331          79892
```

### hipcc

hip currently uses a vendored hipcc binary in hip/bin.
Is building hipcc (repo not in the manifest) required by Debian?
It compiles fine AFAIK.

### rocm-opencl (including icd) + rocclr

There is a build-dependency circular reference between rocm-opencl
(-runtime, not the deprecated -compiler) and rocm-clr.
CMake generally broken here. Hacked in respective CMakeLists.txt :

rocclr:

```
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime/khronos/headers/opencl2.2" )
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime/khronos/headers/" )
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime" )
```

=> Produces a librocclr.a that you copy to your /usr/lib

rocm-opencl-runtime:

```
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime/khronos/headers/opencl2.2")
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime/khronos")
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime/khronos/headers")
include_directories("/home/maxzor/rocm/amd_sources/radeonopencompute/ROCm-OpenCL-Runtime")
include_directories("/home/maxzor/rocm/amd_sources/rocm-developer-tools/ROCclr/include/")
include_directories("/home/maxzor/rocm/amd_sources/rocm-developer-tools/ROCclr")
include_directories("/home/maxzor/rocm/amd_sources/rocm-developer-tools/ROCclr/elf")
include_directories("/home/maxzor/rocm/amd_sources/rocm-developer-tools/ROCclr/device/gpu/gslbe/src/rt/")
```

=> Produces target common
./khronos/icd/libOpenCL.so
./tools/cltrace/libcltrace.so
./tools/clinfo/clinfo
./amdocl/libamdocl64.so

## ROCk

Is amdgpu <==> ROCk?
(Is KFD fully in AMDGPU now?)

My compiled rocminfo says:

```
maxzor@maxdeb:~$ sudo rocminfo
ROCk module is loaded
KFD does not support xnack mode query.
ROCr must assume xnack is disabled.
HSA Error:  Incompatible kernel and userspace, AMD Radeon VII disabled. Upgrade amdgpu.
terminate called after throwing an instance of 'std::bad_function_call'
  what():  bad_function_call
Aborted
```

# 2. wonders for Debian DM-DDs

Isn't debuild supposed to run tests?

No points in having roct-thunk/tests/kfdtest because it depends on sp3 (blob)

Build thunk static or dynamic? I went for shlib.

amd_device_libs bitcode = proprietary blobs?
How is it different from the firmware .bin microcode? no... compiled shaders?

In libhsa-runtime debs, why no libhsa-runtime64-config.cmake to
/usr/lib/x86_64-linux-gnu/cmake/hsa-runtime64 ?

Thinking about onomastics. Better HSA_RUNTIME than HSA-RUNTIME !(? I think not)
no runtime"64" - replace with arch ?
Have the debian package names be the
1.cmake targets 2.repo names 3 debianized names?

Install dirs?
/usr/lib/x86_64-linux-gnu/libhsakmt.so
/usr/lib/libhsa-runtime64.so

Libhsa runtime dev why checksums on all but .so's?

## Project management

### Current tools

Repo: salsa

# TODO

Blob list
Task board: Trello? https://trello.com/b/NX4ERZEE/main

# Ideas

- SQLite database cloc urls... cf .csv?
- Integration with draw.io dep graph (apt-rdepends + vcg not super sexy)?
- Git repo for ROCm is great!
  Salsify [git-repo](https://gerrit.googlesource.com/git-repo/), or the contrary?
- Work on live branches rather than snapshots? Dunno how with gbp.
- CI/CD? https://about.gitlab.com/blog/2016/10/12/automated-debian-package-build-with-gitlab-ci/

# 20211221 Jitsi

Hello,
We were three participants last Tuesday to the jitsi session :
Cordell, Etienne and me. I'll try my best at transcripting the conversation
topics, feel free to correct or add to it.
I have put an [update] tag on some points.

- Maxime: Are tests done by gbp buildpackage, sbuild, debuild...?
  It seems that at least some tests are not run.
  Etienne: Yes they are, but heuristics to find the tests may be limited :
  in this case override dh_auto_test in debian/rules.
  You can skip build-dependency testing with <no-check> in debian/control.

- Etienne: Which architectures are targeted?
  Cordell: for now x86_64 is supported, not sure about aarch64 or others.
  Etienne: The Debian build server could be a good way to see where we're at.

- Cordell wanted to know how the uploads were happening.
  Etienne gave an overview of Debian FTP master's job.

- Maxime: What naming scheme for the packages?
  Cordell: Programs and utilities generally uppercase ROC, rest in lowercase.
  The contrary for libraries.
  Etienne: In general, stick to upstream naming. Some ecosystems like java,
  python, perl... have naming policies.
  [update] Maxime: What about all-lowercase? Starting with rocm- for all?
  rocm-cmake, rocm-hipamd, rocm-hip-blas... ?

- Cordell: AMD fellow Jeremy Newton, packager for Fedora, is knowledgeable
  about the lower part of the ROCm stack.

The the discussion went on the next steps:

- I will push the changes made at https://gitlab.com/rocm_salsa
  to personal salsa repos as soon as I get a login, and pull request
  to the team's salsa repos afterwards.
  [update] the 7 packages rocm-{cmake, comgr,
  device-libs, smi-lib, info, runtime, thunk} + hipamd with Cory's script all
  compile on debian testing with llvm-13, without llvm-project nor clang forks
  from AMD (!). _So far_, there is no need to package these forks anymore.
  Still working a first iteration of a hipamd package.

- Etienne will proceed to send the packages to FTP masters gradually, starting
  with `rocm-cmake`. He might revisit the copyright files which are TODO.
  We will keep building the stack up with the participants to the team.

[update] I wonder Cordell if you have a docker-compose at hand,
I am trying to figure out a workflow with git-repo,
the debian packaging tools and docker here :).

Thank you Etienne and Cordell for your time and warm welcome!
BR, Maxime

---

Consider adding issues to salsa repos.

# rant

There was interest in supporting bundles 10 years ago https://lists.debian.org/debian-devel/2010/09/msg00208.html
With dh_components. It was mostly dismissed. Enrico Weigelt closed the conversation with "
Another point: from users/sysop's view, I'd really like to have packages
as small as possible, and only those installed which are really required.
IIRC these bundles will essentially make few big distro packages out
of dozens small upstream packages - exactly the opposite of what I want."
The main use case evoked in this mail thread was small perl modules.
However I have exactly the same problem, where developers do not go for the git submodule approach, but instead for the opposite multiple "git repos bundle", in a big software stack.
https://github.com/ROCm-Developer-Tools/HIPamd The install process is here https://gist.github.com/cgmb/73086a32258934b1eb1f15340304ace6#file-ce-rocm-build-sh-L73
They have made it quite very hard to compile and install each repo target individually

# general wonders

DCMAKE_HIP_ARCHITECTURES is made mandatory by upstream cmake in hipamd CMakeLists.txt.
Patch out the flag, and make a general package... or make a package per target GPU?

## Dependency graph
How does one compute a minimal dependency graph?
There are various dependency layers, each declared dep can be a false positive upon the lowest layer...

- Debian depends
- Cmake find_package
- C/C++ include
- Symbol call

- Shlib symbols?

## Install paths

I opinate to go full multi-arch. Currently installs are made by packages
both multiarch (/usr/lib/<triplet>) and classic (/usr/lib).
For the case where someone wants to cross-compile ROCm for
for aarch64 for raspbian - raspberry pi, for ppc64, RISCV...

  - /bin
  - /usr/lib/x86_64-linux-gnu/rocm/ Multi-arch-ready object files
    A rocm directory, to glob all rocm entries together, is legitimate,
    while waiting for some harmonization of rocm shared library sonames.
    I'd recommend "libroc{m,k,t}-<package>.so" .
  - /usr/lib/x86_64-linux-gnu/cmake/rocm/ # Package configuration files
  - /usr/include/<triplet>/<rocm>/<package>
  - /usr/share/rocm/amdgcn/ for device libs bitcode. Which is architecture
    independent in the context of CPU.
  - rest (/usr/share/doc, /etc, .pc...)

A quick solution, and an alternative to getting AMD to implement those changes in the build system generator right away, is to:
  1. patch the find-package cmake calls in config-mode -
     add in the `NAMES` list of ARGN "rocm",
  2. adapt the .install debian files to adhere to the way described above.
     This does not prevents from pushing for changes upstream.

20211230
  I've built "rocm-all" from Cordell's script once: it all worked, takes like 3 hours to run the attached full-build script.
  Testing with llvm-13 from this base TODO

## Misc TODO

Compile Cory+llvm-13
Force clang instead of gcc in all packages and compilations
Go back to Matt llvm irc

CMake target --> SONAME --> binary package name


1. If /usr/lib/<triplet> is for multiarch support, and /usr/share is for arch independent binaries, why are there still package installs in /usr/lib?
Should all new packages go to /usr/lib/<triplet> or /usr/share/<package>?
2. Are all the packages that install in /usr/lib, /usr/lib/<package> and /usr/lib/<triplet>/<package>, instead of just /usr/lib/<triplet> bad packages? Is there some sort of policy extension for this rule?
3. Pkg-config and CMake files are configuration files by definition: shouldn't they go in /etc?
4. What is the status for llvm-13 for example? Quite everything (includes, libs, share, bins, cmake) is in some sort of chroot dir /usr/lib/llvm-13? Is this a new norm?

Compiler stack security and hardening inspiration
   /usr/bin/hipcc   -g -O2 -ffile-prefix-map=/debamd/rocrand/rocrand=. -fstack-protector-strong -Wformat -Werror=format-security -Wdate-time -D_FORTIFY_SOURCE=2  -o CMakeFiles/cmTC_a4bf9.dir/testCXXCompiler.cxx.o -c /debamd/rocrand/rocrand/obj-x86_64-linux-gnu/CMakeFiles/CMakeTmp/testCXXCompiler.cxx

