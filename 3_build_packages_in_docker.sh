#!/bin/bash
# -------------------------------- Script setup -------------------------------
# Ordered repo list
repos=("rocm-cmake" "roct-thunk-interface" "rocm-device-libs" "rocm-smi-lib" \
    "rocr-runtime" "rocminfo" "rocm-compilersupport" "rocm-hipamd" \
    "rocrand" "rocprim" "rocblas" "rocsolver" "hipsolver" "hipblas" \
    "rocsparse" "hipsparse" "rocfft" "hipfft" "rocalution" "rocthrust")
# echo "repos: ${repos[@]}"

# Help/error message
usage() { echo -e "\nUsage: \e[1m$0\e[0m [\e[1m-i\e[0m] [\e[1m-f\e[0m] [\e[1m-t\e[0m]\n\n \
    \e[1mOPTIONS\e[0m \n \
        \e[1m-i\e[0m \n \
            Install after building. Yes by default, if this flag is set, will not install. \n \
        \e[1m-f\e[0m \n \
            Install from <package_name>. Start of the list by default. \n \
        \e[1m-t\e[0m \n \
            Install until <package_name>. End of the list by default. \n\n \
    \e[1mORDERED PACKAGE LIST\e[0m \n \
        ${repos[@]} \n\n \
    \e[1mEXAMPLES\e[0m \n \
        ./3_build_packages_in_docker.sh -i n -f rocminfo -t rocm-hipamd\n" \
    1>&2; exit 1; }

# Argument parsing
while getopts ":f:t:i:" o; do
    case "${o}" in
        f)
            f=${OPTARG}
            if [[ ! " ${repos[@]} " =~ " ${f} " ]]; then usage; fi;
            ;;
        t)
            t=${OPTARG}
            if [[ ! " ${repos[@]} " =~ " ${t} " ]]; then usage; fi;
            ;;
        i)
            i=${OPTARG}
            ;;
        *)
            usage
            ;;
    esac
done

# Compute the range in the "repos" package array that we build
# what if I had written python...
j=0
for repo in ${repos[@]}
do :
    if [[ $repo == $f ]]; then
        break
    elif [ -z ${f+x} ]; then
        break
    else
        let "j++"
    fi
done

n=${#repos[@]}
k=0
for repo in ${repos[@]}
do :
    if [[ $repo == $t ]]; then
        break
    else
        let "k++"
    fi
done

# Create the convenience output places
mkdir -p /debamd/packaging_output/deb
mkdir -p /debamd/packaging_output/build_lintian

# Workaround to llvm-defaults(:14) being a bit broken
if [ ! -f "/usr/bin/clang" ]; then
    sudo ln -s /usr/bin/clang-14 /usr/bin/clang
fi
if [ ! -f "/usr/bin/clang-offload-bundler" ]; then
    sudo ln -s /usr/bin/clang++-14 /usr/bin/clang++
fi
if [ ! -f "/usr/bin/clang-offload-bundler" ]; then
    sudo ln -s /usr/bin/clang-offload-bundler-14 /usr/bin/clang-offload-bundler
fi

# ------------------------------- Build packages ------------------------------
# Debug / info
echo "All repos: ${repos[@]}"
echo "n $n | f $f $j | t $t $k"
# the range thing is in bash :origin:length
echo "Targets: ${repos[@]:j:k+1-j}"

CC=clang
CXX=clang++

#for repo in `find . -maxdepth 1 -type d ! -name "rocm-comgr" `
# the order matters here, for build-dependencies.
for repo in ${repos[@]:j:k+1-j}
do :
    printf '%.s-' {1..80} && printf '\n'
    echo Building $repo
    printf '%.s-' {1..80} && printf '\n'

    cd /debamd/$repo/$repo
    git checkout pristine-tar && git checkout upstream && git checkout master

    # build the source tarball
    # bug with MUT packages
    # if [[ "$repo" == "rocm-hipamd" ]] || [[ "$repo" == "rocblas" ]]; then
    #     uscan --overwrite-download
    # else
        origtargz
    # fi

    # build. This is annoying,
    # some patches are not correctly undone/ignored by gbp
    # (mostly when it crashes and does not clean) in this case you probably
    # have to manually intervene...
    # need to clean either in d/clean or in d/rules such as with execute_before_dh_clean
    # Update: don't, python would need a build watchdog cleaning upon crash, meanwhile
    # Andrey Rakhmatullin recommended to copy the source dir with this --git-export-dir trick.
    gbp buildpackage --git-ignore-new --git-export-dir=/debamd/$repo || exit

    # At the end of the build, we have such a file layout:
    # root@35f75b12da9d:/debamd# tree -L 2
    # .
    # |-- 3_build_packages_in_docker.sh
    # |-- rocm-cmake
    # |   |-- rocm-cmake                            *Source* for this package
    # |   |-- rocm-cmake-4.5.2                      Source dir used to build by gbp
    # |   |-- rocm-cmake-4.5.2.tar.gz               Original gzipped source
    # |   |-- rocm-cmake_4.5.2-1.debian.tar.xz      Debian source tarball
    # |   |-- rocm-cmake_4.5.2-1.dsc                Package metadata (hashes)
    # |   |-- rocm-cmake_4.5.2-1_all.deb            *Binary* package  
    # |   |-- rocm-cmake_4.5.2-1_amd64.build        Build log
    # |   |-- rocm-cmake_4.5.2-1_amd64.buildinfo    Build metadata
    # |   |-- rocm-cmake_4.5.2-1_amd64.changes      Changelog metadata (hashes)
    # |   `-- rocm-cmake_4.5.2.orig.tar.gz -> rocm-cmake-4.5.2.tar.gz
    # `-- roct-thunk-interface
    #     |-- libhsakmt-dev_4.5.2+dfsg-1_amd64.deb
    #     |-- libhsakmt1-dbgsym_4.5.2+dfsg-1_amd64.deb
    #     |-- libhsakmt1_4.5.2+dfsg-1_amd64.deb
    #     |-- roct-thunk-interface                  Here lies the source for this package
    # |   |-- roct-thunk-interface-4.5.2            Source dir used to build by gbp
    #     |-- roct-thunk-interface-4.5.2+dfsg       Repacked by d/rules uupdate because of copyright issues
    #     |-- roct-thunk-interface-4.5.2.tar.gz
    #     |-- roct-thunk-interface_4.5.2+dfsg-1.debian.tar.xz
    #     |-- roct-thunk-interface_4.5.2+dfsg-1.dsc
    #     |-- roct-thunk-interface_4.5.2+dfsg-1_amd64.build
    #     |-- roct-thunk-interface_4.5.2+dfsg-1_amd64.buildinfo
    #     |-- roct-thunk-interface_4.5.2+dfsg-1_amd64.changes
    #     `-- roct-thunk-interface_4.5.2+dfsg.orig.tar.xz

# ------------------------------ Optional install -----------------------------
    # install the package(s) built if the short option has been set "-i something"
    if [ -z ${i+x} ]; then
        printf '%.s-' {1..80} && printf '\n'
        echo Installing $repo
        printf '%.s-' {1..80} && printf '\n'
        sudo dpkg -i ../*.deb
    fi
# ------------------------------ Output production ----------------------------
    cp ../*.deb /debamd/packaging_output/deb
    cp ../*.build /debamd/packaging_output/build_lintian
    cd /debamd
done
