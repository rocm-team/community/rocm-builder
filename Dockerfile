FROM debian:unstable
ENV DEBIAN_FRONTEND noninteractive
WORKDIR /debamd
# ------------------------------- Dependencies --------------------------------
    # Base tooling
RUN apt-get update && apt-get install -qq build-essential vim apt-utils curl \
    jq libdrm-dev tree ncdu ripgrep mc wget pkg-config cmake python3-pip dh-exec \
    # Debian packaging tools
    devscripts git-buildpackage sbuild apt-rdepends piuparts autopkgtest lintian \
    # yes, I really tried sbuild inside docker...
    decopy sbuild-debian-developer-setup \
    # There must be lighter latex.
    doxygen dot2tex fragmaster texmaker \
    # ROCm build dependencies 
    llvm-15-dev clang-15 libclang-15-dev clang-tools-15 lld-15 liblld-15-dev \
    kmod msr-tools libelf-dev zlib1g-dev libnuma-dev libudev-dev mesa-common-dev \
    python3.11-venv python3-virtualenv python3-yaml python3-msgpack gfortran \
    libmsgpack-dev libfmt-dev

# ------------------------------- Environment ---------------------------------
RUN apt-get update && apt-get install -qq sudo
RUN adduser --disabled-password --gecos '' myuser
RUN adduser myuser sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> \
    /etc/sudoers

# Pervase UID/GID
COPY ./salsa/*id.txt /home/myuser/
# This was hackily written by ./2_*.sh
RUN usermod -u $(cat /home/myuser/uid.txt) myuser
RUN adduser myuser sbuild
RUN adduser myuser video
RUN groupadd render
RUN groupmod -g $(cat /home/myuser/render_gid.txt) render
RUN adduser myuser render

# -------------------------------- File setup ---------------------------------
COPY ./misc/dotfiles/* /home/myuser/
# -------------------------------- Misc ---------------------------------
USER myuser
