#!/bin/bash

# Send this script in the container root dir,
# where it is supposed to be executed.
cp ./3_build_packages_in_docker.sh salsa/.

# Because on permissions, docker and host sync on UID/GID, not names...
# Our uid will be reused by the docker user 'myuser'
# This is to ease copying and tweaking files on both sides of the wall
# We will reuse the "render" gid too to access the GPU without root.
# #permissionmanagementdonebadprobably
echo $(id | sed -n 's/^uid=\([0-9]*\).*/\1/p') > salsa/uid.txt
echo $(id | sed -n 's/.*,\([0-9]*\)(render).*$/\1/p') > salsa/render_gid.txt

# ------------------------------- Docker setup --------------------------------
docker-compose build
docker-compose run --rm debamd
