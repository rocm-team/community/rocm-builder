# What

This is a set of shell scripts and docker files, \
for a mini Debian-packaging factory.

# Why

The goal is to help if you look into packaging ROCm on Debian, and provide a \
prepared sandbox to do so. \
Docker creates an isolated environment where you won't mess up your daily \
config files, libraries... \
The layered filesystem of docker also helps not reinstalling everything \
every time.

# How

The scripts here, for now, build a docker image with Debian unstable, \
and the required build dependencies, from scratch once, which takes time. \
Next docker launches use the cache.

The first lowest third of the ROCm stack can currently be built and installed, \
with a dockerized development environment, running :

```bash
./1_import_salsa_repos.sh -r

# maxzor@ws:~/rocm/debian_packaging/builder$ ./1_import_salsa_repos.sh -h
# Usage: ./1_import_salsa_repos.sh [-r --repo] [-h --help]
# Download ROCm Debian package sources from salsa, either with curl and
# jq (default), or with git-repo (package in Debian contrib):
#     './1_install_salsa_repos.sh -r'.


./2_setup_docker.sh
./3_build_packages_in_docker.sh

# myuser@debamd:/debamd$ ./3_build_packages_in_docker.sh -help

# Usage: ./3_build_packages_in_docker.sh [-i] [-f] [-t]

#      OPTIONS 
#          -i 
#              Install after building. Yes by default, if this flag is set, will not install. 
#          -f 
#              Install from <package_name>. Start of the list by default. 
#          -t 
#              Install until <package_name>. End of the list by default. 

#      ORDERED PACKAGE LIST 
#          rocm-cmake roct-thunk-interface rocm-device-libs rocm-smi-lib rocr-runtime rocminfo rocm-compilersupport rocm-hipamd rocrand rocprim rocblas rocsolver hipsolver hipblas rocsparse hipsparse rocfft hipfft rocalution rocthrust 

#      EXAMPLES 
#          ./3_build_packages_in_docker.sh -i n -f rocminfo -t rocm-hipamd

```

Currently, the following 10 packages are compiled and installed (ROCm version 4.5.2):

- rocm-cmake
- roct-thunk-interface
- rocm-device-libs
- rocm-smi-lib
- rocr-runtime
- rocminfo
- rocm-compilersupport
- rocm-hipamd
- rocrand
- rocblas

Mapping AMD-Debian, source-binary

|AMD source          |AMD binary             |Debian source       |Debian binary                                        |
|--------------------|-----------------------|--------------------|-----------------------------------------------------|
|rocm-cmake          |?                      |rocm-cmake          |rocm-cmake                                           |
|rocm-device-libs    |rocm-device-libs       |rocm-device-libs    |rocm-device-libs                                     |
|rocm_smi_lib        |?                      |rocm-smi-lib        |librocm-smi64 librocm-smi-dev liboam-1 liboam-dev    |
|roct-thunk-interface|hsakmt-roct-amdgpu     |roct-thunk-interface|libhsakmt1 libhsakmt-dev                             |
|rocr-runtime        |hsa-runtime-rocr-amdgpu|rocr-runtime        |libhsa-runtime64-1 libhsaruntime64-dev               |
|rocminfo            |?                      |rocminfo            |rocminfo                                             |
|rocm-compilersupport|comgr-amdgpu-pro       |rocm-compilersupport|rocm-comgr                                           |
|hipamd              |hip-rocr-amdgpu-pro    |rocm-hipamd         |libamdhip64-4 libamdhip64-dev libhiprtc-builtins-4   |
|rocrand             |rocrand                |rocrand             |librocrand1 librocrand-dev libhiprand1 libhiprand-dev|
|rocblas             |rocblas                |rocblas             |librocblas1 librocblas-dev                           |

# Ouptut

The `salsa` directory gets bind-mounted in the container, which means that \
the content of the package builds become available outside of the container. \
The .debs and lintian reports are stacked in `salsa/packaging_output` \
for convenience.

# Ideas

## DockerHub

An option could be to add container images to dockerhub at \
different stages of the installation of the ROCm stack:

- base
- first third (stack installed up until rocr-runtime + compiler support)
- second third (hip "sub-platform" installed)
- rocm-all (higher libraries installed)

See also: https://github.com/RadeonOpenCompute/ROCm-docker

## Git-repo+

[Git-repo](https://gerrit.googlesource.com/git-repo) is advertised in the official ROCm documentation as the way to \
manage the numerous git repositories of the stack locally. It does so using \
an xml manifest at the [root repo](https://github.com/RadeonOpenCompute/ROCm) (default.xml).

If Debian packaging goes well and sees updates in future ROCm versions, \
it could be nice to tweak git-repo to work with two xml manifests :

- one for AMD ROCm
- one for Debian salsa ROCm

A mapping between the two manifests would be used by this "tweaked-repo":

| Debian       | <-- -- -->                        | AMD                      |
| ------------ | --------------------------------- | ------------------------ |
| rocm-smi-lib | 1-1 mapping                       | rocm_smi_lib             |
| corectrl     | 1-0 mapping                       | -                        |
| hipamd       | 1-n mapping                       | hipamd, hip, clr, opencl |
| -            | 0-1 mapping (in AMD manifest)     | llvm-project             |
| hcc          | 1-1 mapping (not in AMD manifest) | hcc                      |

## 20211228 Stop this factory at all costs now, before it arhhhghh-

I only knew about debuild when starting this. \
This mini-project is basically reinventing sbuild (which uses schroot and \
debootstrap), with the usually divisive nuance between chroot and docker ; \
and a fortiori this is reinventing [due](https://github.com/CumulusNetworks/DUE). \
The last thing Debian needs might be a bazillion-th build environment.

See also: https://wiki.debian.org/Packaging/Pre-Requisites \
Bare metal is quite wrong unless you never make mistakes. \
To isolate yourself, I advise against virtual machines, (vbox is a no-no) \
because we do GPGPU here, and setting up GPU passthrough can be tricky.

# Reuse

If you ever were to reuse this, you would start by editing your \
custom info in the misc/dotfiles: .bashrc, .gitconfig, ...
